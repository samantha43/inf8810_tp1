# PROJET I DE TRAITEMENT ET ANALYSE DE DONNEES MASSIVES
Disponile à l'adresse [https://gitlab.com/Darwin99/inf8810_tp1/-/blob/main/map_reduce_sparksql_tp1_inf8810_dyst.ipynb?ref_type=heads](https://gitlab.com/Darwin99/inf8810_tp1/-/blob/main/map_reduce_sparksql_tp1_inf8810_dyst.ipynb?ref_type=heads) ou à [https://gitlab.com/Darwin99/inf8810_tp1/-/blob/main/README.md?ref_type=heads](https://gitlab.com/Darwin99/inf8810_tp1/-/blob/main/README.md?ref_type=heads)

## Auteurs
- [DARIL RAOUL KENGNE WAMBO](  <a href="mailto:kengne_wambo.daril_raoul@courrier.uqam.ca">**```KEND82330000```**</a> )
- [TINHINANE BOUDIAB](  <a href="mailto:boudiab.tinhinane@courrier.uqam.ca">**```BOUT79360000```**</a> )
- [YVES FERSTLER](  <a href="mailto:ferstler.yves@courrier.uqam.ca">**```FREY```**</a> )
- [OHAN SAMANTHA SONFACK ANEWOU](  <a href="mailto:sonfack_anewou.johan_samantha@courrier.uqam.ca">**```SONJ86350009```**</a> )

## Description de la source de données

La source de données est un fichier csv de nom [**```ouverture_de_donnees_affichages_postulants.csv```**](ouverture_de_donnees_affichages_postulants.csv) disponible à l'adresse suivante : https://donnees.montreal.ca/dataset/offres-demploi-et-postulation. Ce fichier contient les données sur les offres d'emploi et les postulations de la ville de Montréal. Il est composé de 10 colonnes et de 1000 lignes. Les colonnes sont les suivantes comme fournies dans [le site de la ville de Montréal](https://donnees.montreal.ca/dataset/offres-demploi-et-postulation):
<!-- UNITE_NIVEAU1,DESCRIPTION_UNITE_NIVEAU1,UNITE,DESCRIPTION_UNITE,ACCREDITATION,DESCRIPTION_ACCREDITATION,TITRE,EMPLOI,NO_AFFICHAGE,DEBUT,FIN,INTERNE_EXTERNE,NOMBRE_POSTULANT,NOMBRE_FEMME,NOMBRE_HANDICAPE,NOMBRE_MINORITE_VISIBLE,NOMBRE_AUTOCHTONE,NOMBRE_MINORITE_ETHNIQUE -->


- **```Unite_Niveau1```**(Numérique) :  L'unité de niveau 1 représente le service ou l'arrondissement
- **```Description_Unite_Niveau1```**(Texte variable) : Description de l'unité de niveau 1
- **```Unite```** (Numérique) : L'unité administrative est l'unité d'affaires où l'offre d'emploi a été créée. Elle est représentée par 12 chiffres significatifs dont les 2 premiers représentent le service ou l'arrondissement. La numérotation représente une hiérarchie de la structure (ex.: Service, direction, division, section, ...)

- **`Description_Unite`** (Texte variable) : Description de l'unité administrative

- **`Accreditation`** (Numérique) : Accréditation de l'emploi de l'offre d'emploi. Elle est représentée par 2 chiffres.

- **`Description_Accreditation`** (Texte variable) : Description de l'accréditation

- **`Titre`** (Texte variable) : Description de l'emploi

- **`Emploi`** (Numérique) : Code d'emploi de l'offre d'emploi. Il est représenté par 6 chiffres.

- **`No_Affichage`** (Texte variable) : Numéro de l'offre d'emploi. C'est un champ alphanumérique avec un standard défini pour sa nomenclature. Ex.: FIN-16-TEMP-344210-1 Veut dire que c'est une offre d'emploi au service des finances faite en 2016 pour un poste temporaire sur l'emploi 344210. Dans certains cas le numéro de poste est aussi indiqué.

- **`Debut`** (Date) : Date de début de l'affichage

- **`Fin`** (Date) : Date de fin de l'affichage

- **`Interne_Externe`** (Texte variable) : Indicateur permettant de savoir si c'est un affichage ouvert à l'interne exclusivement.

- **`Nombre_Postulant`** (Numérique) : Nombre total de postulants ayant postulé sur cet affichage

- **`Nombre_Femme`** (Numérique) : Nombre de femmes ayant postulé sur cet affichage

- **`Nombre_Handicape`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant que personne handicapée

- **`Nombre_Minorite_Visible`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant que minorité visible

- **`Nombre_Autochtone`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant qu'autochtone

- **`Nombre_Minorite_Ethnique`** (Numérique) : Nombre de personnes s'étant auto-identifiées en tant que minorité ethnique

## Visualisation des données
![image](Images/data_preview.png)




## Prétraitement des données
```python
# Installation de la bibliotheque
! pip install pyspark
! pip install plotly_express
```

### 1. Création d'une instannce spark session
```python
#Importation des bibliotheques
from pyspark.sql import SparkSession
from pyspark.sql.functions import to_date, col,count,isnan, when
import plotly.express as px
# Créer une instance spark session
spark = SparkSession.builder.appName("Pretraitement_pyspark").getOrCreate()
```

### 2. Lectures de données
```python
# Lecture du dataset
df = spark.read.options(delimiter=",", header=True).csv("ouverture_de_donnees_affichages_postulants.csv")
# Affichage de 5 premieres ligne du dataset
df.show(5,truncate=False)
```

```text
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+
|UNITE_NIVEAU1|DESCRIPTION_UNITE_NIVEAU1                                   |UNITE       |DESCRIPTION_UNITE                      |ACCREDITATION|DESCRIPTION_ACCREDITATION|TITRE                                   |EMPLOI|NO_AFFICHAGE               |DEBUT     |FIN       |INTERNE_EXTERNE|NOMBRE_POSTULANT|NOMBRE_FEMME|NOMBRE_HANDICAPE|NOMBRE_MINORITE_VISIBLE|NOMBRE_AUTOCHTONE|NOMBRE_MINORITE_ETHNIQUE|
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+
|37           |service de police de la ville de montreal                   |370105050000|soutien aux enquêtes                   |12           |fonctionnaires           |operateur(-trice) de video-clavier - cum|713260|SPVM-09-TEMP-713260-15889  |2009-07-02|2009-07-08|Interne        |0               |0           |0               |0                      |0                |0                       |
|37           |service de police de la ville de montreal                   |370105050000|soutien aux enquêtes                   |12           |fonctionnaires           |secretaire d unite administrative       |791930|SPVM-09-TEMP-791930-15888  |2009-07-02|2009-07-08|Interne        |0               |0           |0               |0                      |0                |0                       |
|28           |service des infrastructures/transport et environnement      |281008000000|division administration - environnement|12           |fonctionnaires           |commis de pesee                         |754810|SITE-09-BHRS-754810*       |2009-07-02|2009-07-08|Interne/Externe|0               |0           |0               |0                      |0                |0                       |
|59           |cote-des-neiges - notre-dame-de-grâce                       |590202000000|division des ressources humaines       |59           |prof/blanc/annk          |bibliothecaire - occasionnel(le)        |720790|CDNNDG-09-temp-720790*     |2009-07-03|2009-07-09|Interne/Externe|1               |1           |0               |0                      |0                |0                       |
|35           |dev culturel,qualite milieu de vie,diversite ethnoculturelle|350803000000|division du soutien aux evenements     |12           |fonctionnaires           |secretaire d unite administrative       |791930|DCQMVDE-09-TEMP-791930-3086|2009-07-02|2009-07-09|Interne        |0               |0           |0               |0                      |0                |0                       |
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+
only showing top 5 rows
```

### 3. Prétraitement de la donnée

Cette partie consiste à faire un prétraitement de la donnée en vue de la transformer en un format exploitable par Spark SQL. Pour cela, nous avons procédé comme suit:
#### 3.1. Calcule des valeurs nulls
```python
# Afficher pour chaque colonne le nombre de valeurs nulls (resultat sous format dictionnaire)
# Ceci nous permet de voir les colonnes qui contiennent des valeurs nulls
Dict_Null = {col:df.filter(df[col].isNull()).count() for col in df.columns}

Dict_Null
```


```text
{'Unite_Niveau1': 0,
 'DESCRIPTION_UNITE_NIVEAU1': 0,
 'UNITE': 0,
 'DESCRIPTION_UNITE': 0,
 'Accreditation': 0,
 'DESCRIPTION_ACCREDITATION': 0,
 'TITRE': 0,
 'EMPLOI': 0,
 'NO_AFFICHAGE': 0,
 'debut': 0,
 'fin': 0,
 'INTERNE_EXTERNE': 0,
 'Nombre_Postulant': 0,
 'Nombre_Femme': 0,
 'Nombre_Handicape': 0,
 'Nombre_Minorite_Visible': 0,
 'Nombre_Autochtone': 0,
 'Nombre_Minorite_Ethnique': 0}

```
#### 3.2 Suppression des valeurs nulls du dataset
```python
# Suppression des valeurs nulls dans le dataset
df = df.na.drop()
```
#### 3.3 Affichage du schéma du dataset

```python
# Affichage du Schéma du dataset (Colonnes + DataType)
df.printSchema()
```

```text
root
 |-- UNITE_NIVEAU1: string (nullable = true)
 |-- DESCRIPTION_UNITE_NIVEAU1: string (nullable = true)
 |-- UNITE: string (nullable = true)
 |-- DESCRIPTION_UNITE: string (nullable = true)
 |-- ACCREDITATION: string (nullable = true)
 |-- DESCRIPTION_ACCREDITATION: string (nullable = true)
 |-- TITRE: string (nullable = true)
 |-- EMPLOI: string (nullable = true)
 |-- NO_AFFICHAGE: string (nullable = true)
 |-- DEBUT: string (nullable = true)
 |-- FIN: string (nullable = true)
 |-- INTERNE_EXTERNE: string (nullable = true)
 |-- NOMBRE_POSTULANT: string (nullable = true)
 |-- NOMBRE_FEMME: string (nullable = true)
 |-- NOMBRE_HANDICAPE: string (nullable = true)
 |-- NOMBRE_MINORITE_VISIBLE: string (nullable = true)
 |-- NOMBRE_AUTOCHTONE: string (nullable = true)
 |-- NOMBRE_MINORITE_ETHNIQUE: string (nullable = true)

```
#### 3.4 Changement de type de colonnes

Ceci nous servira dans la section 3.5

```python
# les colonnes Debut et fin doivent être des dates car actuellement ils sont de type string
df = df.withColumn("debut", to_date(df["debut"], "yyyy-MM-dd")).withColumn("fin",to_date(df["fin"],"yyyy-MM-dd"))
```
```python
"""
Les colonnes Unite_Niveau1, Unite, Nombre_Minorite_Ethnique, Nombre_Autochtone,
Nombre_Minorite_Visible, Nombre_Handicape, Nombre_Femme, Nombre_Postulant, Emploi, Accreditation doivent etre numeriques
"""
columns_to_num = ['Unite_Niveau1', 'Nombre_Minorite_Ethnique', 'Nombre_Autochtone',
'Nombre_Minorite_Visible', 'Nombre_Handicape', 'Nombre_Femme', 'Nombre_Postulant', 'Accreditation']
for colonne in columns_to_num:
  df= df.withColumn(colonne,col(colonne).cast("int"))
```

```python
# Re-vérification des types
df.printSchema()
```
```text
root
 |-- Unite_Niveau1: integer (nullable = true)
 |-- DESCRIPTION_UNITE_NIVEAU1: string (nullable = true)
 |-- UNITE: string (nullable = true)
 |-- DESCRIPTION_UNITE: string (nullable = true)
 |-- Accreditation: integer (nullable = true)
 |-- DESCRIPTION_ACCREDITATION: string (nullable = true)
 |-- TITRE: string (nullable = true)
 |-- EMPLOI: string (nullable = true)
 |-- NO_AFFICHAGE: string (nullable = true)
 |-- debut: date (nullable = true)
 |-- fin: date (nullable = true)
 |-- INTERNE_EXTERNE: string (nullable = true)
 |-- Nombre_Postulant: integer (nullable = true)
 |-- Nombre_Femme: integer (nullable = true)
 |-- Nombre_Handicape: integer (nullable = true)
 |-- Nombre_Minorite_Visible: integer (nullable = true)
 |-- Nombre_Autochtone: integer (nullable = true)
 |-- Nombre_Minorite_Ethnique: integer (nullable = true)

```

#### 3.5 Vérification de la cohérence du nombre de postulant et de la diversité

Verifions maintenant que pour chaque ligne, le nombre totale de postulant est égale à la somme des postulants par sexe, par minorité visible, par minorité ethnique et par autochtone.

##### 3.5.1 Création d'une colonne somme

```python
# construction d'une colonne qui contient la somme des valeurs des colonnes Nombre_Minorite_Ethnique, Nombre_Autochtone, Nombre_Minorite_Visible, Nombre_Handicape, Nombre_Femme
df = df.withColumn("Nombre_Total", df["Nombre_Minorite_Ethnique"] + df["Nombre_Autochtone"] + df["Nombre_Minorite_Visible"] + df["Nombre_Handicape"] + df["Nombre_Femme"])
df.show(5, truncate=False)
```
```text
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
|Unite_Niveau1|DESCRIPTION_UNITE_NIVEAU1                                   |UNITE       |DESCRIPTION_UNITE                      |Accreditation|DESCRIPTION_ACCREDITATION|TITRE                                   |EMPLOI|NO_AFFICHAGE               |debut     |fin       |INTERNE_EXTERNE|Nombre_Postulant|Nombre_Femme|Nombre_Handicape|Nombre_Minorite_Visible|Nombre_Autochtone|Nombre_Minorite_Ethnique|Nombre_Total|
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
|37           |service de police de la ville de montreal                   |370105050000|soutien aux enquêtes                   |12           |fonctionnaires           |operateur(-trice) de video-clavier - cum|713260|SPVM-09-TEMP-713260-15889  |2009-07-02|2009-07-08|Interne        |0               |0           |0               |0                      |0                |0                       |0           |
|37           |service de police de la ville de montreal                   |370105050000|soutien aux enquêtes                   |12           |fonctionnaires           |secretaire d unite administrative       |791930|SPVM-09-TEMP-791930-15888  |2009-07-02|2009-07-08|Interne        |0               |0           |0               |0                      |0                |0                       |0           |
|28           |service des infrastructures/transport et environnement      |281008000000|division administration - environnement|12           |fonctionnaires           |commis de pesee                         |754810|SITE-09-BHRS-754810*       |2009-07-02|2009-07-08|Interne/Externe|0               |0           |0               |0                      |0                |0                       |0           |
|59           |cote-des-neiges - notre-dame-de-grâce                       |590202000000|division des ressources humaines       |59           |prof/blanc/annk          |bibliothecaire - occasionnel(le)        |720790|CDNNDG-09-temp-720790*     |2009-07-03|2009-07-09|Interne/Externe|1               |1           |0               |0                      |0                |0                       |1           |
|35           |dev culturel,qualite milieu de vie,diversite ethnoculturelle|350803000000|division du soutien aux evenements     |12           |fonctionnaires           |secretaire d unite administrative       |791930|DCQMVDE-09-TEMP-791930-3086|2009-07-02|2009-07-09|Interne        |0               |0           |0               |0                      |0                |0                       |0           |
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
only showing top 5 rows

```
##### 3.5.2 Nombre de lignes où la somme est inférieure au nombre de postulant

Dans ce cas, c'est normal que la somme soit inférieure au nombre de postulant car il y a des postulants qui ne se sont pas identifiés.

```python
df.where(df["Nombre_Total"] <= df["Nombre_Postulant"]).show(5, truncate=False)
nombre_coherence = df.where(df["Nombre_Total"] <= df["Nombre_Postulant"]).count()
nombre_coherence
```
```text
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
|Unite_Niveau1|DESCRIPTION_UNITE_NIVEAU1                                   |UNITE       |DESCRIPTION_UNITE                      |Accreditation|DESCRIPTION_ACCREDITATION|TITRE                                   |EMPLOI|NO_AFFICHAGE               |debut     |fin       |INTERNE_EXTERNE|Nombre_Postulant|Nombre_Femme|Nombre_Handicape|Nombre_Minorite_Visible|Nombre_Autochtone|Nombre_Minorite_Ethnique|Nombre_Total|
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
|37           |service de police de la ville de montreal                   |370105050000|soutien aux enquêtes                   |12           |fonctionnaires           |operateur(-trice) de video-clavier - cum|713260|SPVM-09-TEMP-713260-15889  |2009-07-02|2009-07-08|Interne        |0               |0           |0               |0                      |0                |0                       |0           |
|37           |service de police de la ville de montreal                   |370105050000|soutien aux enquêtes                   |12           |fonctionnaires           |secretaire d unite administrative       |791930|SPVM-09-TEMP-791930-15888  |2009-07-02|2009-07-08|Interne        |0               |0           |0               |0                      |0                |0                       |0           |
|28           |service des infrastructures/transport et environnement      |281008000000|division administration - environnement|12           |fonctionnaires           |commis de pesee                         |754810|SITE-09-BHRS-754810*       |2009-07-02|2009-07-08|Interne/Externe|0               |0           |0               |0                      |0                |0                       |0           |
|59           |cote-des-neiges - notre-dame-de-grâce                       |590202000000|division des ressources humaines       |59           |prof/blanc/annk          |bibliothecaire - occasionnel(le)        |720790|CDNNDG-09-temp-720790*     |2009-07-03|2009-07-09|Interne/Externe|1               |1           |0               |0                      |0                |0                       |1           |
|35           |dev culturel,qualite milieu de vie,diversite ethnoculturelle|350803000000|division du soutien aux evenements     |12           |fonctionnaires           |secretaire d unite administrative       |791930|DCQMVDE-09-TEMP-791930-3086|2009-07-02|2009-07-09|Interne        |0               |0           |0               |0                      |0                |0                       |0           |
+-------------+------------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+----------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
only showing top 5 rows

27756
```

##### 3.5.3 Nombre de lignes où la somme est supérieure au nombre de postulant

Dans ce cas, c'est anormal que la somme soit supérieure au nombre de postulant car donc il y a des incohérences dans les données.

```python
df.where(df["Nombre_Total"] > df["Nombre_Postulant"]).show(5, truncate=False)
nombre_incoherence = df.where(df["Nombre_Total"] > df["Nombre_Postulant"]).count()
nombre_incoherence
```
```text
+-------------+------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+-------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
|Unite_Niveau1|DESCRIPTION_UNITE_NIVEAU1                             |UNITE       |DESCRIPTION_UNITE                      |Accreditation|DESCRIPTION_ACCREDITATION|TITRE                                |EMPLOI|NO_AFFICHAGE               |debut     |fin       |INTERNE_EXTERNE|Nombre_Postulant|Nombre_Femme|Nombre_Handicape|Nombre_Minorite_Visible|Nombre_Autochtone|Nombre_Minorite_Ethnique|Nombre_Total|
+-------------+------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+-------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
|87           |montreal-nord                                         |870202000000|division des ressources humaines       |12           |fonctionnaires           |agent(e) d approvisionnement niveau 1|700720|MN-09-TEMP-700720-32349    |2009-07-08|2009-07-14|Interne        |3               |1           |0               |3                      |0                |0                       |4           |
|59           |cote-des-neiges - notre-dame-de-grâce                 |590202000000|division des ressources humaines       |12           |fonctionnaires           |agent(e) du cadre bâti               |782930|CDNNDG-09-TEMP-782930      |2009-07-15|2009-07-22|Interne/Externe|4               |2           |0               |2                      |0                |1                       |5           |
|28           |service des infrastructures/transport et environnement|280201000000|direction de la réalisation des travaux|12           |fonctionnaires           |secretaire de direction              |791840|SITE-09-VACA-791840-39509  |2009-07-16|2009-07-22|Interne        |1               |1           |0               |1                      |0                |0                       |2           |
|28           |service des infrastructures/transport et environnement|280201000000|direction de la réalisation des travaux|12           |fonctionnaires           |prepose(e) a la documentation        |792430|site-09-VACA-792430-36540  |2009-07-21|2009-07-28|Interne        |21              |15          |0               |4                      |0                |3                       |22          |
|52           |ville-marie                                           |520202000000|employés en réaffectation              |12           |fonctionnaires           |agent(e) de communications sociales  |706310|VM-VMA-09-TEMP-706310-41246|2009-07-22|2009-07-28|Interne        |1               |1           |0               |1                      |0                |0                       |2           |
+-------------+------------------------------------------------------+------------+---------------------------------------+-------------+-------------------------+-------------------------------------+------+---------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+------------+
only showing top 5 rows

18573
```
```python
print(f"Donc au total le pourcentage d'incohérence est de {round((nombre_incoherence/(nombre_coherence+nombre_incoherence))*100,2)}%")
```
```text
Donc au total le pourcentage d'incohérence est de 40.09%
```

```python
# Calculer les statistiques descriptives
# Ceci nous permet de voir les valeurs min, max, moyenne, ecart type, etc.
df['NOMBRE_POSTULANT',	'NOMBRE_FEMME','NOMBRE_HANDICAPE',	'NOMBRE_MINORITE_VISIBLE',	'NOMBRE_AUTOCHTONE',	'NOMBRE_MINORITE_ETHNIQUE'].describe().toPandas()
```

<!-- insert the images -->
![image](Images/metrics.png)

Ces statistiques servent à donner une vue globale sur le nombre des differents condidants postulant à des differents types d'emploi.
#### 3.6 Affichage des valeurs uniques des colonnes categorielle
Etant donné que le pretraitement est ouvert, nous utiliserons  des vues temporaires pour faire des requêtes SQL sur le dataset. et plus tard dans le traitement, nous utiliserons des dataframes seulement.
```python
# Creation d'une vue temporaire afin d'analyser les colonnes du dataset en detail

df.createOrReplaceTempView("emploi")
```
```python
# Affichage des DESCRIPTION_ACCREDITATION unique de dataset
spark.sql("SELECT DISTINCT DESCRIPTION_ACCREDITATION FROM emploi").show()   
```
```text
[Stage 5:===========================================================(3 + 0) / 3]
+-------------------------+
|DESCRIPTION_ACCREDITATION|
+-------------------------+
|           fonctionnaires|
|           professionnels|
|           pro.scien.p.e.|
|          prof/blanc/annk|
|          cadre direction|
|          cadre administ.|
|          elus municipaux|
|            stages emploi|
|          manuels c.bleus|
|                 pompiers|
|          pers. politique|
|             pro.juristes|
|              architectes|
|          contremaitre ca|
|            contremaitres|
|          contrac/ns/ocpm|
|           brig.scolaires|
|                policiers|
|          etat major pom.|
|           av.not.n.synd.|
+-------------------------+

                                                                                
```
```python
# Affichage des valeurs differentes de la colonne Interne_externe
spark.sql("SELECT DISTINCT INTERNE_EXTERNE FROM emploi").show()
```
```text
+---------------+
|INTERNE_EXTERNE|
+---------------+
|        Interne|
|Interne/Externe|
+---------------+

```
```python
# Affichage des niveau d'unité differentes dans le dataset
spark.sql("SELECT DISTINCT DESCRIPTION_UNITE_NIVEAU1  FROM emploi").show(truncate=False)
```
```text
+--------------------------------------------------------+
|DESCRIPTION_UNITE_NIVEAU1                               |
+--------------------------------------------------------+
|communications et relations avec les citoyens           |
|depenses communes                                       |
|arrondissement de verdun                                |
|rivière-des-prairies - pointe-aux-trembles              |
|affaires juridiques                                     |
|service du matériel roulant et des ateliers             |
|service de l'eau                                        |
|mise en valeur du territoire et du patrimoine           |
|commission de la fonction publique de montreal          |
|bureau du taxi de montréal - paramunicipal              |
|service des finances                                    |
|service des communications                              |
|montreal-nord                                           |
|direction des systemes d'information                    |
|service de la qualité de vie                            |
|3erv. des infrastructures,de la voirie et des transports|
|l'île bizard - ste-geneviève                            |
|service des ressources humaines                         |
|service des affaires juridiques                         |
|cote-des-neiges - notre-dame-de-grâce                   |
+--------------------------------------------------------+
only showing top 20 rows

```
Nous remarquons que la colonne DESCRIPTION_UNITE_NIVEAU1 comporte à la fois les services et les arrondissements.

```python
# Extraire les unites niveau1 qui ne sont pas des servises
spark.sql("SELECT DISTINCT DESCRIPTION_UNITE_NIVEAU1  FROM emploi where DESCRIPTION_UNITE_NIVEAU1 not like 'service%'").show(100,truncate=False)
```
```text
[Stage 14:>                                                         (0 + 3) / 3]
+------------------------------------------------------------+
|DESCRIPTION_UNITE_NIVEAU1                                   |
+------------------------------------------------------------+
|communications et relations avec les citoyens               |
|depenses communes                                           |
|arrondissement de verdun                                    |
|rivière-des-prairies - pointe-aux-trembles                  |
|affaires juridiques                                         |
|mise en valeur du territoire et du patrimoine               |
|commission de la fonction publique de montreal              |
|bureau du taxi de montréal - paramunicipal                  |
|montreal-nord                                               |
|direction des systemes d'information                        |
|3erv. des infrastructures,de la voirie et des transports    |
|l'île bizard - ste-geneviève                                |
|cote-des-neiges - notre-dame-de-grâce                       |
|st-laurent                                                  |
|planification stratégique et performance organisationnelle  |
|pierrefonds - roxboro                                       |
|bureau inspecteur général                                   |
|affaires corporatives                                       |
|ahuntsic-cartierville                                       |
|sud-ouest                                                   |
|saint-léonard                                               |
|rosemont - la petite-patrie                                 |
|anjou                                                       |
|ombudsman de montreal                                       |
|mercier - hochelaga-maisonneuve                             |
|plateau-mont-royal                                          |
|dev culturel,qualite milieu de vie,diversite ethnoculturelle|
|ville-marie                                                 |
|villeray - st-michel - parc-extension                       |
|secretariat de liaison de l'agglomeration de montreal       |
|direction générale                                          |
|agence de mobilité durable                                  |
|lachine                                                     |
|bureau de la vérificatrice générale                         |
|outremont                                                   |
|lasalle                                                     |
+------------------------------------------------------------+

                                                                                
```
```python
# Recuperer la liste des arrondissements
list_arrondissement = ['arrondissement de verdun','rivière-des-prairies - pointe-aux-trembles','montreal-nord',"l'île bizard - ste-geneviève"
'cote-des-neiges - notre-dame-de-grâce', 'st-laurent','pierrefonds - roxboro','ahuntsic-cartierville','sud-ouest',
'saint-léonard', 'rosemont - la petite-patrie','anjou'
'ombudsman de montreal', 'mercier - hochelaga-maisonneuve'
'plateau-mont-royal','ville-marie','villeray - st-michel - parc-extension','lachine','outremont','lasalle']
```
```python
# Recuprer la données sur les arrondissement
df_filtered = df.filter(col("DESCRIPTION_UNITE_NIVEAU1").isin(list_arrondissement))
df_filtered.show(5,truncate=False)
```
```python
df_filtered.show(5,truncate=False)
```
```text
+-------------+-------------------------+------------+-------------------------------------+-------------+-------------------------+-------------------------------------+------+-------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+
|Unite_Niveau1|DESCRIPTION_UNITE_NIVEAU1|UNITE       |DESCRIPTION_UNITE                    |Accreditation|DESCRIPTION_ACCREDITATION|TITRE                                |EMPLOI|NO_AFFICHAGE             |debut     |fin       |INTERNE_EXTERNE|Nombre_Postulant|Nombre_Femme|Nombre_Handicape|Nombre_Minorite_Visible|Nombre_Autochtone|Nombre_Minorite_Ethnique|
+-------------+-------------------------+------------+-------------------------------------+-------------+-------------------------+-------------------------------------+------+-------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+
|85           |saint-léonard            |850202010000|section bibliothèque                 |12           |fonctionnaires           |aide-bibliothecaire                  |763810|SLE-09-TEMP-763810-39889*|2009-07-02|2009-07-09|Interne/Externe|0               |0           |0               |0                      |0                |0                       |
|82           |pierrefonds - roxboro    |820205000000|division des ressources humaines     |29           |professionnels           |charge(e) de communication           |406810|ROPI-09-PERM-406810      |2009-07-02|2009-07-10|Interne/Externe|1               |0           |0               |0                      |0                |0                       |
|53           |sud-ouest                |530202000000|division du greffe                   |14           |manuels c.bleus          |prepose(e) aux travaux generaux      |600530|SO-09-BPRE-600530B       |2009-07-06|2009-07-10|Interne/Externe|0               |0           |0               |0                      |0                |0                       |
|82           |pierrefonds - roxboro    |820205000000|division des ressources humaines     |29           |professionnels           |architecte paysagiste                |455810|ROPI-09-VACA-455810-32600|2009-07-02|2009-07-13|Interne/Externe|0               |0           |0               |0                      |0                |0                       |
|89           |lasalle                  |890200000000|direction des services administratifs|12           |fonctionnaires           |technicien(ne) en ressources humaines|720550|LAS-09-TEMP-720550       |2009-07-07|2009-07-14|Interne/Externe|2               |1           |0               |1                      |0                |0                       |
+-------------+-------------------------+------------+-------------------------------------+-------------+-------------------------+-------------------------------------+------+-------------------------+----------+----------+---------------+----------------+------------+----------------+-----------------------+-----------------+------------------------+
only showing top 5 rows

```




## Traitement MapReduce

Nous aimerons savoir **la proportion de diversité(femme,handicapée,  minorité visible, autochtone et minorité ethnique) de postulants par emploi**. Pour cela, nous allons suivre un ensemble d'étapes pour arriver à notre résultat à l'aide d'Hadoop streaming avec python.
### Mapper
Le mapper va nous permettre de faire un premier traitement sur les données. Il va nous permettre de récupérer les données qui nous intéressent ici un couple **(emploie ,titre_emploi__nombre_postulant__diversite)** provenant d'un filtre qu'on fera sur chaque tuple de notre jeu de données retenant ceux ou le champ NOMBRE_POSTULANT est non null ou superieure à 0 . 
Pour cela, nous allons utiliser le langage python pour écrire notre mapper. Le code est disponible dans le fichier [**```mapper.py```**](mapper.py). Le code est le suivant :

```python
#!/usr/bin/env python
import sys

# Pour chaqu'emploie, on voudrait le pourcentage de la diversit qui est la somme de NOMBRE_FEMME, NOMBRE_HANDICAPE, NOMBRE_MINORITE_VISIBLE, NOMBRE_AUTOCHTONE, NOMBRE_MINORITE_ETHNIQUE divise par  NOMBRE_POSTULANT


for line in sys.stdin:
    # on saute la premiere ligne
    if line.startswith("UNITE_NIVEAU1"):
        continue
    # on split la ligne
    line = line.strip()
    line = line.split(",")
    if len(line) != 18:
        continue
    # on recupere les colonnes qui nous interesse
    emploi = line[7].strip('"') # on enleve les guillemets
    titre_emploi = line[6].strip('"')
    nombre_postulant = line[12].strip('"')
    nombre_femme = line[13].strip('"')
    nombre_handicape = line[14].strip('"')
    nombre_minorite_visible = line[15].strip('"')
    nombre_autochtone = line[16].strip('"')
    nombre_minorite_ethnique = line[17].strip('"')
    # on fait le calcul
    # on fait attention a ne pas diviser par 0
    if nombre_postulant == "0":
        continue
    diversite = (int(nombre_femme) + int(nombre_handicape) + int(nombre_minorite_visible) + int(nombre_autochtone) + int(nombre_minorite_ethnique)) 
    # on affiche le resultat
    #On affichele code de l'emploie(la cle), le titre de l'emploie(car il peut arriver que le meme emploi ait des description differente), le nombre de postulant et le nombre de diversite
    print( "%s\t%s" % (emploi, titre_emploi+"__"+nombre_postulant+ "__" + str(diversite)))
```
### Reducer
Le reducer va permettre de faire le second tratitement en regroupant le nombre de diversite de tous les emplois, puis en calculant la proportion de la diversite pour chaqu'emploi. Le code est disponible dans le fichier [**```reducer.py```**](reducer.py). Le code est le suivant :

```python
#!/usr/bin/env python
import sys

# Le format dans lequel on recoit les donnees est emploi\ttitre_emploi__nombre_postulant__diversite
# Nous allons donc construire une map qui a comme cle l'emploi et comme valeur une liste de tuple (titre_emploi, nombre_postulant, diversite)
result_map = {}


for line in sys.stdin:
    line = line.strip()
    emploi, titre_emploi_nombre_postulant_diversite = line.split("\t")
    (
        titre_emploi,
        nombre_postulant,
        diversite,
    ) = titre_emploi_nombre_postulant_diversite.split("__")
    if emploi in result_map:
        result_map[emploi].append((titre_emploi, nombre_postulant, diversite))
    else:
        result_map[emploi] = [(titre_emploi, nombre_postulant, diversite)]

# On peut calculer la moyenne de la diversite pour chaque emploi et afficher le resultat
for emploi in result_map:
    # On calcule la moyenne
    moyenne = 0
    for titre_emploi, nombre_postulant, diversite in result_map[emploi]:
        moyenne += float(diversite)
    if len(result_map[emploi]) != 0: # on fait attention a ne pas diviser par 0
        moyenne /= len(result_map[emploi])
        # On affiche le resultat emploi-titre_emploi\tmoyenne
        # On afficher la moyenne avec 2 chiffres apres la virgule

        print("%s\t%s" % (emploi+"-"+titre_emploi, str(moyenne)[:4]))

```
## Execution du job MapReduce

### Lancement du cluster Hadoop


Pour lancer le conteneur cloudera, entrer la commande suivante une fois positionné dans le dossier **inf8810_tp1_dyst** où se trouve le fichier [**```build_and_lunch_cloudera.sh```**](build_and_lunch_cloudera.sh):

```bash
./build_and_lunch_cloudera.sh
``` 

```bash
#!/bin/bash 
# Afficher en vert le texte
GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
NOCOLOR='\033[0m'

# Afficher en vert  cyan le texte

echo -e "${CYAN}TP1: Construction du conteneur"
echo -e "${NOCOLOR}"
# aller a la ligne
echo ""
docker build -t cloudera_dyst .

echo -e "${GREEN}TP1: Conteneur construit \n"

echo -e "${CYAN}TP1: Lancement du conteneur et execution du job map reduce \n\n"

docker run --name cloudera_dyst --hostname=quickstart.cloudera --privileged=true -ti cloudera_dyst  /usr/bin/docker-quickstart
echo -e "${NOCOLOR}\n\n"
```

Ce script nous permet d'éffectuer deux opérations:
- **Construire l'image docker de notre job MapReduce**
  L'idée ici est de copier les données et les scripts dans l'image docker. Pour cela, nous avons utilisé le fichier [**```Dockerfile```**](Dockerfile) qui est le suivant :

```dockerfile
FROM cloudera/quickstart:latest
# On copie nos scripts et données dans le container
COPY ./ /root
```
- **Lancer le job MapReduce**
```bash
docker run --name cloudera_dyst --hostname=quickstart.cloudera --privileged=true -ti cloudera_dyst  /usr/bin/docker-quickstart

```
Cette commande va ouvrir le conteneur en tant que root en mode interactif et avec comme nom d'hôte **quickstart.cloudera**.

Patientez quelques minutes le temps que le conteneur se lance.



### Lancement du job MapReduce

Une fois le conteneur lancé, le terminal s'est ouvert, positionnez vous dans le dossier **/root** dans lequel les scripts et données ont été copiés. Pour cela, **entrez la commande suivante :**

```bash
cd /root
```

**Puis, entrez la commande suivante pour lancer le job MapReduce :**

```bash
./run_map_reduce_job.sh
```

Ce fichier est le suivant :

```bash
#!/bin/bash  -x

GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
NOCOLOR='\033[0m'

#1.positionnement dans le repertoire /root/ qui a ete binde dans le docker-compose.yml
WORKDIR=/root
cd $WORKDIR
#2. Soyons sure que le fichier(ouverture_de_donnees_affichages_postulants.csv) existe sur hdfs
# En se referant a la documentation https://hadoop.apache.org/docs/r1.2.1/file_system_shell.html
echo -e "${CYAN}On verifie si le fichier ouverture_de_donnees_affichages_postulants.csv existe sur hdfs\n\n"



hdfs dfs -test -e ouverture_de_donnees_affichages_postulants.csv

# si le fichier existe deja sur hdfs, on ne fait rien, on continu sinon on le televerse sur hdfs
if [ $? -eq 0 ]; then
    echo -e "${GREEN}Skiping file upload...\n"
    echo -e "${GREEN}The file ouverture_de_donnees_affichages_postulants.csv exists"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file ouverture_de_donnees_affichages_postulants.csv does not exist on hdfs, uploading it..."
    echo -e "${NOCOLOR}\n"


    hdfs dfs -put "${WORKDIR}/ouverture_de_donnees_affichages_postulants.csv" 

    
    echo -e "${GREEN}The file ouverture_de_donnees_affichages_postulants.csv has been uploaded"
    echo -e "${NOCOLOR}\n"
fi




#on fait pareille pour les fichiers mapper et reducer
echo -e "${CYAN}"

hdfs dfs -test -e mapper.py

if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload..."
    echo -e "${GREEN}The file mapper.py exists\n\n"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file mapper.py does not exist on hdfs, uploading it...\n"
    echo -e "${CYAN}\n"

    hdfs dfs -put "${WORKDIR}/mapper.py" 

    echo -e "${GREEN}The file mapper.py has been uploaded\n\n"
    echo -e "${NOCOLOR}"
fi

hdfs dfs -test -e reducer.py
if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload...\n"
    echo -e "${GREEN}The file reducer.py exists\n"
    echo -e "${NOCOLOR}\n"
else
    echo -e "${YELLOW}The file reducer.py does not exist on hdfs, uploading it..."
    echo -e "${CYAN}\n\n"
    

    hdfs dfs -put "${WORKDIR}/reducer.py" 

    echo -e "${GREEN}The file reducer.py has been uploaded\n"
    echo -e "${NOCOLOR}\n\n"
    
fi


#3. On s'assure que le fichier de resultat n'existe pas deja sur hdfs
hdfs dfs -test -e /user/cloudera/resultat_traitement_1
if [ $? -eq 0 ]; then
    echo -e "${CYAN}Skiping file upload..."
    echo -e "${GREEN}The file resultat_traitement_1 exists"
    echo -e "${CYAN}\n\n"

    hdfs dfs -rm -r /user/cloudera/resultat_traitement_1


    echo -e "${GREEN}The file resultat_traitement_1 has been deleted"
    echo -e "${NOCOLOR}\n\n"
else
    echo -e "${GREEN}The file resultat_traitement_1 does not exist on hdfs, skiping..."
    echo -e "${NOCOLOR}\n\n"
fi

#3. On lance le job map reduce

echo -e "${CYAN}TP1: Lancement du job map reduce du calcul du nombre de femme qui postulent par emploi"

hadoop jar '/usr/lib/hadoop-mapreduce/hadoop-streaming.jar' -file 'mapper.py' -mapper mapper.py -file 'reducer.py' -reducer reducer.py -input 'ouverture_de_donnees_affichages_postulants.csv' -output '/user/cloudera/resultat_traitement_1'

if [ $? -eq 0 ]; then
    echo -e "${GREEN}The job map reduce has been executed successfully\n\n"
    # Affichage du resultat
    echo -e "${CYAN}TP1: Affichage du resultat\n\n"

    hdfs dfs -cat /user/cloudera/resultat_traitement_1/part-00000

    echo -e "${NOCOLOR}"
    echo -e "${GREEN}TP1: Fin du traitement 1 \n\n"
    echo -e "${GREEN}Thaks! BY DYST\n\n"
    echo -e "${NOCOLOR}"
else
    echo -e "${RED}The job map reduce has failed"
    echo -e "${NOCOLOR}"
fi
```



## Traitements Spark SQL
Nous allons, dès à présent, utiliser SparkSQL pour approfondir notre analyse. Notre projet va se baser sur la durée des offres d’emplois selon chaque secteur. Cela va nous permettre de distinguer les emplois offrant les plus longues durées de travail en jours.

Cependant avant d'entrer dans l'analyse des données nous avons des prérequis à satisfaire.

### Prérequis

Afin d'utiliser Spark SQL nous devons nous assurer d'installer PySpark

```python 
# !pip install pyspark  # si ce n'est pas déjà fait
```

Nous devons également importer des dépendances:

```python
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, sum, desc, datediff, avg
from pyspark.sql.types import StructType, StructField, StringType, IntegerType, DateType
```

Nous initialisons une nouvelle session Spark en utilisant le constructeur, en attribuant le nom 'project_pyspark' à notre application, et nous récupérons la session existante ou en créons une nouvelle si elle n'existe pas encore.Ceci juste dans le but de séparerer les traitements du prétraitement fait plus haut.

```python
spark = SparkSession.builder.appName("project_pyspark").getOrCreate()
```

Nous définissons la politique du parseur de temps à "LEGACY" pour la configuration de Spark afin de gérer les formats de date et d'heure de manière correcte.


```python
spark.conf.set("spark.sql.legacy.timeParserPolicy", "LEGACY")
```
Finalement, on peut définir un schema pour représenter notre source de données et créer une instance Spark.

```python
schema = StructType([
    StructField("Unite_Niveau1", IntegerType(), False), # False car la colonne ne peut pas etre null d´après le pré
    StructField("Description_Unite_Niveau1", StringType(), False),
    StructField("Unite", IntegerType(), False),
    StructField("Description_Unite", StringType(), False),
    StructField("Accreditation", IntegerType(), False),
    StructField("Description_Accreditation", StringType(), False),
    StructField("Titre", StringType(), False),
    StructField("Emploi", IntegerType(), False),
    StructField("No_Affichage", StringType(), False),
    StructField("Debut", DateType(), False),
    StructField("Fin", DateType(), False),
    StructField("Interne_Externe", StringType(), False),
    StructField("Nombre_Postulant", IntegerType(), False),
    StructField("Nombre_Femme", IntegerType(), False),
    StructField("Nombre_Handicape", IntegerType(), False),
    StructField("Nombre_Minorite_Visible", IntegerType(), False),
    StructField("Nombre_Autochtone", IntegerType(), False),
    StructField("Nombre_Minorite_Ethnique", IntegerType(), False)
])

# Lecture de la source de données et initialisation de l'instance Spark
data = spark.read.csv('ouverture_de_donnees_affichages_postulants.csv', header=True, schema=schema)

#Affichage des types du schema créé
data.printSchema()
```

```text
root
 |-- Unite_Niveau1: integer (nullable = true)
 |-- Description_Unite_Niveau1: string (nullable = true)
 |-- Unite: integer (nullable = true)
 |-- Description_Unite: string (nullable = true)
 |-- Accreditation: integer (nullable = true)
 |-- Description_Accreditation: string (nullable = true)
 |-- Titre: string (nullable = true)
 |-- Emploi: integer (nullable = true)
 |-- No_Affichage: string (nullable = true)
 |-- Debut: date (nullable = true)
 |-- Fin: date (nullable = true)
 |-- Interne_Externe: string (nullable = true)
 |-- Nombre_Postulant: integer (nullable = true)
 |-- Nombre_Femme: integer (nullable = true)
 |-- Nombre_Handicape: integer (nullable = true)
 |-- Nombre_Minorite_Visible: integer (nullable = true)
 |-- Nombre_Autochtone: integer (nullable = true)
 |-- Nombre_Minorite_Ethnique: integer (nullable = true)
```

Une fois ces étapes terminées, on peut passer à l'analyse plus concrète.


## Traitement des secteurs d'emploi offrant les durées (en jours) de travail les plus longues


```python
# Calcul de la durée pour chaque emploi
duration = data.withColumn("Duration", datediff(col("Fin"), col("Debut")))

# Groupé par secteur et moyenne de la durée
average = duration.groupBy("Description_Unite_Niveau1")\
    .agg(avg("Duration").alias("Duration_Moyenne"))\
    .orderBy("Duration_Moyenne", ascending=False)

# Affichage du résultat
average.show(truncate=False)

```
```text
[Stage 6:>                                                          (0 + 3) / 3]
+----------------------------------------------------------+------------------+
|Description_Unite_Niveau1                                 |Duration_Moyenne  |
+----------------------------------------------------------+------------------+
|bureau de la vérificatrice générale                       |18.425925925925927|
|service des ressources humaines                           |17.89566929133858 |
|service de la stratégie immobilière                       |14.909090909090908|
|service du matériel roulant et des ateliers               |14.763440860215054|
|service des technologies de l'information                 |14.576881720430107|
|agence de mobilité durable                                |14.440816326530612|
|commission de la fonction publique de montreal            |14.357142857142858|
|planification stratégique et performance organisationnelle|13.045454545454545|
|service du développement économique                       |12.581005586592179|
|service de l'espace pour la vie                           |12.379095163806552|
|service de l'eau                                          |12.205199628597958|
|service de la gestion et de la planification des immeubles|12.171914893617021|
|service de l'urbanisme et de la mobilité                  |12.076719576719576|
|depenses communes                                         |12.031446540880504|
|service des grands parcs, du mont-royal et des sports     |11.968063872255488|
|l'île bizard - ste-geneviève                              |11.73090909090909 |
|st-laurent                                                |11.57327839905827 |
|service de la diversité et de l'inclusion sociale         |11.563106796116505|
|direction générale                                        |11.457956015523934|
|lasalle                                                   |11.200762388818298|
+----------------------------------------------------------+------------------+
only showing top 20 rows

```









