#!/usr/bin/env python
import sys

# Pour chaqu'emploie, on voudrait le pourcentage de la diversit qui est la somme de NOMBRE_FEMME, NOMBRE_HANDICAPE, NOMBRE_MINORITE_VISIBLE, NOMBRE_AUTOCHTONE, NOMBRE_MINORITE_ETHNIQUE divise par  NOMBRE_POSTULANT


for line in sys.stdin:
    # on saute la premiere ligne
    if line.startswith("UNITE_NIVEAU1"):
        continue
    # on split la ligne
    line = line.strip()
    line = line.split(",")
    if len(line) != 18:
        continue
    # on recupere les colonnes qui nous interesse
    emploi = line[7].strip('"') # on enleve les guillemets
    titre_emploi = line[6].strip('"')
    nombre_postulant = line[12].strip('"')
    nombre_femme = line[13].strip('"')
    nombre_handicape = line[14].strip('"')
    nombre_minorite_visible = line[15].strip('"')
    nombre_autochtone = line[16].strip('"')
    nombre_minorite_ethnique = line[17].strip('"')
    # on fait le calcul
    # on fait attention a ne pas diviser par 0
    if nombre_postulant == "0":
        continue
    diversite = (int(nombre_femme) + int(nombre_handicape) + int(nombre_minorite_visible) + int(nombre_autochtone) + int(nombre_minorite_ethnique)) 
    # on affiche le resultat
    #On affichele code de l'emploie(la cle), le titre de l'emploie(car il peut arriver que le meme emploi ait des description differente), le nombre de postulant et le nombre de diversite
    print( "%s\t%s" % (emploi, titre_emploi+"__"+nombre_postulant+ "__" + str(diversite)))